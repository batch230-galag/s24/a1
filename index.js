/*
Activity:
1. In the S24 folder, create an activity folder and an index.html and index.js file inside of it.
2. Link the script.js file to the index.html file.

14. Create a git repository named S24.
15. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
16. Add the link in Boodle.
*/


/*
3. Create a variable getCube and use the exponent operator to compute for the cube of a number. (A cube is any number raised to 3)
4. Using Template Literals, print out the value of the getCube variable with a message of The cube of <num> is…
*/

let getCube = (num) => {
    return num ** 3;
}
let num = 2;
console.log(`The cube of ${num} is ${getCube(2)}`);

/*
5. Create a variable address with a value of an array containing details of an address.
6. Destructure the array and print out a message with the full address using Template Literals.
*/

let address = ["258 Washington Ave NW", "California", "90011"];
let [avenueName, countryName, zipCode] = address;

console.log(`I live at ${avenueName}, ${countryName} ${zipCode}`);

/*
7. Create a variable animal with a value of an object data type with different animal details as it’s properties.
8. Destructure the object and print out a message with the details of the animal using Template Literals.
*/

const animal = {
    name: "Lolong",
    type: "Saltwate Crocodile",
    weight: "1075 kgs",
    measurement: "20ft 3in"
};

const {name, type, weight, measurement} = animal;
console.log(`${name} was a ${type}. He weighed at ${weight} with a measurement of ${measurement}`);


/*
9. Create an array of numbers.
10. Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.
*/

let numbers = [1, 2, 3, 4, 5];
numbers.forEach((num) => {
    console.log(num);
})

/*
11. Create a variable reduceNumber and using the reduce array method and an arrow function console log the sum of all the numbers in the array.
*/

let reducedNumber = numbers.reduce((num1, num2) => {
    return num1 + num2;
})
console.log(reducedNumber);
/*
12. Create a class of a Dog and a constructor that will accept a name, age and breed as it’s properties.
13. Create/instantiate a new object from the class Dog and console log the object.
*/

class Dog {
    constructor (name, age, breed) {
        this.name = name;
        this.age = age;
        this.breed = breed;
    }
}

let dog = new Dog();
dog.name = "Frankie";
dog.age = 5;
dog.breed = "Miniature Dachshund";

console.log(dog);